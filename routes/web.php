<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\VehiclesController;
use App\Http\Controllers\InmueblesController;

use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RentersController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\WelcomeController;


Auth::routes();

Route::group(['prefix' => 'auth'], function () {
    Route::get('/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', [ WelcomeController::class, 'index' ] );
Route::get('/intro', [ WelcomeController::class, 'intro' ]);
Route::get('/property-type-group', [ WelcomeController::class, 'property_group' ]);
Route::get('/data_categories', [ InmueblesController::class, 'categories' ]);




Route::prefix('inmuebles')->group(function () { 

    /** Vistas Inmuebles **/
    Route::get('/property-type', [ InmueblesController::class, 'index' ]);
    Route::get('/privacy-type', [ InmueblesController::class, 'index' ]);
    Route::get('/location', [ InmueblesController::class, 'index' ]);
    Route::get('/floor-plan', [ InmueblesController::class, 'index' ]);
    Route::get('/amenities', [ InmueblesController::class, 'index' ]);
    Route::get('/photos', [ InmueblesController::class, 'index' ]);
    Route::get('/title', [ InmueblesController::class, 'index' ]);
    Route::get('/description', [ InmueblesController::class, 'index' ]);
    Route::get('/price', [ InmueblesController::class, 'index' ]);
    Route::get('/legal', [ InmueblesController::class, 'index' ]);
    Route::get('/preview', [ InmueblesController::class, 'index' ]);

    /** Consultas **/    
    Route::post('/data_subcategories', [ InmueblesController::class, 'subcategories' ]);
    Route::get('/data_country', [ InmueblesController::class, 'country' ]);
    Route::get('/data_city', [ InmueblesController::class, 'city' ]);
    Route::post('/inmueble_image', [InmueblesController::class, 'UploatImageInmueble']);
    Route::post('/register', [InmueblesController::class, 'PostInmueble']);

});

Route::prefix('vehicles')->group(function () { 

    /** Vistas Turo **/
    Route::get('/list-your-car', [ VehiclesController::class, 'index' ]);


    /** Consultas Turo **/
    Route::post('/register', [ VehiclesController::class, 'PostVehiculo' ]);

});

Route::get('/get_renters', [ WelcomeController::class, 'GetRenters' ] );
Route::post('/search_category', [ WelcomeController::class, 'CardSearch' ] );
Route::post('/search_inmueble', [ WelcomeController::class, 'SearchInmueble' ] );

Route::prefix('currency')->group(function () {

    Route::get('/', [ CurrencyController::class, 'GetCurrency' ]);
    Route::post('/currency', [CurrencyController::class, 'PostCurrency']);

}); 

Route::prefix('country')->group(function () {

    Route::get('/', [ CountryController::class, 'GetCountry' ]);
    Route::post('/country', [CountryController::class, 'PostCountry']);

}); 

Route::prefix('city')->group(function () {

    Route::get('/', [ CityController::class, 'GetCity' ]);
    Route::post('/city', [CityController::class, 'PostCity']);

}); 

Route::prefix('category')->group(function () {

    Route::get('/', [ CategoryController::class, 'GetCategoria' ]);
    Route::post('/category', [CategoryController::class, 'PostCategoria']);

}); 


Route::prefix('article')->group(function () { 

    Route::get('/', [ ArticleController::class, 'GetArticle' ]);
    Route::post('/article', [ArticleController::class, 'PostArticle']);

    Route::get('/img_article', [ ArticleController::class, 'GetImgArticle' ]);
    Route::post('/post_img_article', [ArticleController::class, 'PostImgArticle']);

});

Route::prefix('orders')->group(function () { 

    Route::get('/', [ OrdersController::class, 'GetOrders' ]);
    Route::post('/orders', [OrdersController::class, 'PostOrders']);

});

/** CPanel **/

Route::prefix('cpanel')->group(function(){

    Route::get('/', [ DashboardController::class, 'index' ])->name('cpanel');

    Route::get('/solicitud', function(){
        $title = [
            'primero' => 'Cpanel',
            'segundo' => 'Solicitudes de Renters',
        ];
        return view('solicitud_renters', compact('title'));
    });

    Route::get('/solicitud_renters', [ DashboardController::class, 'solicitudes_renters' ]);
    Route::post('/detalle_renter', [ DashboardController::class, 'detalle_renter' ]);
    Route::post('/autorizacion_renters', [ DashboardController::class, 'autorizacion_renters' ]);


    Route::get('/categorias', function(){
        $title = [
            'primero' => 'Cpanel',
            'segundo' => 'Categorias',
        ];
        return view('categorias', compact('title'));
    });

    Route::get('/get_categorias', [ CategoryController::class, 'GetCategoria' ]);
    Route::post('/post_categorias', [ CategoryController::class, 'PostCategoria' ]);



    Route::get('/subcategorias', function(){
        $title = [
            'primero' => 'Cpanel',
            'segundo' => 'Sub-Categorias',
        ];
        return view('subcategorias', compact('title'));
    });

    Route::get('/get_subcategorias', [ CategoryController::class, 'GetSubCategoria' ]);
    Route::post('/post_subcategorias', [ CategoryController::class, 'PostSubCategoria' ]);


});
