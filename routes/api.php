<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\VehiclesController;
use App\Http\Controllers\API\InmueblesController;

use App\Http\Controllers\API\CurrencyController;
use App\Http\Controllers\API\CountryController;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\RentersController;
use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\OrdersController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\WelcomeController;
use App\Http\Controllers\API\APILoginController;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**Auth de Passport**/
Route::group([
    'prefix' => 'auth'
    ], function () {
    Route::post('/login', [APILoginController::class, 'Login']); 
    Route::post('/register', [APILoginController::class, 'Register']); 

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', [ APILoginController::class, 'Logout']);
        Route::get('user', [ APILoginController::class, 'Logout']);
    });
});

/**Extrae los datos de los renters aprobados**/
Route::get('/get_renters', [ WelcomeController::class, 'GetRenters' ] );
/**Buscar un renter por categoria**/
Route::post('/search_category', [ WelcomeController::class, 'CardSearch' ] );

/**Buscar si un inmueble no esta reservado entre fechas y por ubicacion**/
Route::post('/search_inmueble', [ WelcomeController::class, 'SearchInmueble' ] );

/** Inmuebles **/
Route::prefix('inmuebles')->group(function () { 
    /** Consultas **/    
    Route::post('/data_subcategories', [ InmueblesController::class, 'subcategories' ]);
    Route::get('/data_country', [ InmueblesController::class, 'country' ]);
    Route::get('/data_city', [ InmueblesController::class, 'city' ]);
    Route::post('/inmueble_image', [InmueblesController::class, 'UploatImageInmueble']);
    Route::post('/register', [InmueblesController::class, 'PostInmueble']);

});

/** Vehiculos **/
Route::prefix('vehicles')->group(function () { 
    /** Consultas Turo **/
    Route::post('/register', [ VehiclesController::class, 'PostVehiculo' ]);

});

Route::prefix('currency')->group(function () {

    Route::get('/', [ CurrencyController::class, 'GetCurrency' ]);
    Route::post('/currency', [CurrencyController::class, 'PostCurrency']);

}); 

Route::prefix('country')->group(function () {

    Route::get('/', [ CountryController::class, 'GetCountry' ]);
    Route::post('/country', [CountryController::class, 'PostCountry']);

}); 

Route::prefix('city')->group(function () {

    Route::get('/', [ CityController::class, 'GetCity' ]);
    Route::post('/city', [CityController::class, 'PostCity']);

}); 

Route::prefix('category')->group(function () {

    Route::get('/', [ CategoryController::class, 'GetCategory' ]);
    Route::post('/category', [CategoryController::class, 'PostCategoria']);

}); 

Route::prefix('article')->group(function () { 

    Route::get('/', [ ArticleController::class, 'GetArticle' ]);
    Route::post('/article', [ArticleController::class, 'PostArticle']);

    Route::get('/img_article', [ ArticleController::class, 'GetImgArticle' ]);
    Route::post('/post_img_article', [ArticleController::class, 'PostImgArticle']);

});

Route::prefix('orders')->group(function () { 

    Route::get('/', [ OrdersController::class, 'GetOrders' ]);
    Route::post('/orders', [OrdersController::class, 'PostOrders']);

});