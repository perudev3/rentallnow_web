<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCollectionImgVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_collection_img_vehicles', function (Blueprint $table) {
            $table->increments('collection_img_vehicles_id');
            $table->longText('collection_img_vehicles_url')->nullable();
            $table->integer('vehicles_id')->unsigned();
            $table->timestamps();

            $table->foreign('vehicles_id')->references('vehicles_id')->on('tbl_vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_collection_img_vehicles');
    }
}
