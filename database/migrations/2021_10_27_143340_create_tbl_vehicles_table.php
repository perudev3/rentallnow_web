<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_vehicles', function (Blueprint $table) {
            $table->increments('vehicles_id');
            $table->longText('vehicles_lat')->nullable();
            $table->longText('vehicles_lng')->nullable();
            $table->string('vehicles_year')->nullable();
            $table->integer('vehicles_make')->nullable();
            $table->integer('vehicles_model')->nullable();
            $table->integer('vehicles_odometers')->nullable();
            $table->string('vehicles_transmission')->nullable();
            $table->integer('vehicles_type')->nullable();
            $table->longText('vehicles_maker_value')->nullable();
            $table->integer('vehicles_number')->nullable();
            $table->longText('vehicles_licencia')->nullable();
            $table->longText('vehicles_metas')->nullable();
            $table->date('vehicles_disponibilidad')->nullable();
            $table->longText('vehicles_detalles')->nullable();
            $table->integer('subcategory_id')->unsigned();            
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('subcategory_id')->references('subcategory_id')->on('tbl_subcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_vehicles');
    }
}
