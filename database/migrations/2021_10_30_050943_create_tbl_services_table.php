<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_services', function (Blueprint $table) {
            $table->increments('services_id');
            $table->longText('services_city')->nullable();
            $table->longText('services_lat')->nullable();
            $table->longText('services_lng')->nullable();
            $table->date('services_disponibilidad')->nullable();
            $table->longText('services_titulo')->nullable();
            $table->longText('services_detalles')->nullable();
            $table->decimal('services_price', 10,2)->nullable();
            $table->integer('subcategory_id')->unsigned();            
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('subcategory_id')->references('subcategory_id')->on('tbl_subcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_services');
    }
}
