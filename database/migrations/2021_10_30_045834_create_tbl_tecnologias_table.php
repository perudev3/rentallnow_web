<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblTecnologiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tecnologias', function (Blueprint $table) {
            $table->increments('tecnologias_id');
            $table->longText('tecnologias_city')->nullable();
            $table->longText('tecnologias_lat')->nullable();
            $table->longText('tecnologias_lng')->nullable();
            $table->date('tecnologias_disponibilidad')->nullable();
            $table->longText('tecnologias_titulo')->nullable();
            $table->longText('tecnologias_detalles')->nullable();
            $table->decimal('tecnologias_price', 10,2)->nullable();
            $table->integer('subcategory_id')->unsigned();            
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('subcategory_id')->references('subcategory_id')->on('tbl_subcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tecnologias');
    }
}
