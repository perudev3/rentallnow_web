<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_reservas', function (Blueprint $table) {
            $table->increments('reservas_id');
            $table->integer('reservas_status')->nullable();
            $table->date('reservas_llegada')->nullable();
            $table->date('reservas_salida')->nullable();
            $table->integer('reservas_huespedes')->nullable();
            $table->integer('renters_id')->unsigned();
            $table->timestamps();

            $table->foreign('renters_id')->references('renters_id')->on('tbl_renters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_reservas');
    }
}
