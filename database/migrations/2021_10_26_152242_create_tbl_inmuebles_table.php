<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_inmuebles', function (Blueprint $table) {
            $table->increments('inmuebles_id');
            $table->string('inmuebles_name')->nullable();
            $table->longText('inmuebles_lat')->nullable();
            $table->longText('inmuebles_lng')->nullable();
            $table->date('inmuebles_date')->nullable();
            $table->integer('inmuebles_huespedes')->nullable();
            $table->integer('inmuebles_cama')->nullable();
            $table->integer('inmuebles_habitaciones')->nullable();
            $table->integer('inmuebles_baños')->nullable();
            $table->integer('inmuebles_privado')->nullable();
            $table->longText('inmuebles_modcons')->nullable();
            $table->longText('inmuebles_description')->nullable();
            $table->longText('inmuebles_city')->nullable();
            $table->decimal('inmuebles_price', 10,2)->nullable();
            $table->integer('subcategory_id')->unsigned();            
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('subcategory_id')->references('subcategory_id')->on('tbl_subcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_inmuebles');
    }
}
