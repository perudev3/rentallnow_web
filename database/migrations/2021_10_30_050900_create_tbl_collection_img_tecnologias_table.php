<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCollectionImgTecnologiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_collection_img_tecnologias', function (Blueprint $table) {
            $table->increments('collection_img_tecnologias_id');
            $table->longText('collection_img_tecnologias_url')->nullable();
            $table->integer('tecnologias_id')->unsigned();
            $table->timestamps();

            $table->foreign('tecnologias_id')->references('tecnologias_id')->on('tbl_tecnologias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_collection_img_tecnologias');
    }
}
