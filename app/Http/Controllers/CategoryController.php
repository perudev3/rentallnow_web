<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\tbl_category;
use App\Models\tbl_subcategory;

class CategoryController extends Controller
{
    public function GetCategoria()
    {
        return tbl_category::all();
    }

    public function PostCategoria(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'category_name' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $category = tbl_category::create([
            'category_name' => $request->category_name
        ]);

        if ($category == true) {
            return ['status' => 200, 'message' => 'categoria guardada', 'category' => $category];
        }
        
    }

    public function GetSubCategoria()
    {
        return tbl_subcategory::with('category')->get();
    }

    public function PostSubCategoria(Request $request)
    {
        $subcategory = tbl_subcategory::create([
            'subcategory_name' => $request->subcategory_name,
            'category_id' => $request->category_id,
        ]);

        if ($subcategory == true) {
            return ['status' => 200, 'message' => 'Sub categoria guardada'];
        }
        
    }
}
