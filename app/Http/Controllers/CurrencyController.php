<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\tbl_currency;

class CurrencyController extends Controller
{

    public function PostCurrency(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'currency_name' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $currency = tbl_currency::create([
            'currency_name' => $request->currency_name,
            'currency_abr' => $request->currency_abr,
        ]);

        if ($currency == true) {
            return [ 'status' => 200 , 'message' => 'moneda creada', 'currency' => $currency ];
        } 
            

        
    }

}
