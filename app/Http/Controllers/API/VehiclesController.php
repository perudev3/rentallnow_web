<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\tbl_vehicles;
use App\Models\tbl_collection_img_vehicles;

class VehiclesController extends Controller
{
    public function PostVehiculo(Request $request)
    {
        $date = Carbon::now();
        $user = \Auth::user(); 

        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
            'vehicles_lat' => 'required',
            'vehicles_lng' => 'required',
            'vehicles_year' => 'required',
            'vehicles_make' => 'required',
            'vehicles_model' => 'required',
            'vehicles_odometers' => 'required',
            'vehicles_transmission' => 'required',
            'vehicles_type' => 'required',
            'vehicles_maker_value' => 'required',
            'vehicles_number' => 'required',
            'vehicles_metas' => 'required',
            'vehicles_disponibilidad' => 'required',
            'vehicles_detalles' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $vehiculos = tbl_vehicles::create([
            'vehicles_lat' => $request->vehicles_lat,
            'vehicles_lng' => $request->vehicles_lng,
            'vehicles_year' => $request->vehicles_year,
            'vehicles_make' => $request->vehicles_make,
            'vehicles_model' => $request->vehicles_model,
            'vehicles_odometers' => $request->vehicles_odometers,
            'vehicles_transmission' => $request->vehicles_transmission,
            'vehicles_type' => $request->vehicles_type,
            'vehicles_maker_value' => $request->vehicles_maker_value,
            'vehicles_number' => $request->vehicles_number,
            'vehicles_metas' => $request->vehicles_metas,
            'vehicles_disponibilidad' => Carbon::parse($request->vehicles_disponibilidad),
            'vehicles_detalles' => $request->vehicles_detalles,
            'subcategory_id' => 2,
            'user_id' => $user->id,
        ]);

        $images = $request->file('collection_img_vehicles_url');
        $cont = 0;
        foreach($images as $img){
            $custom_name = 'images-'.Str::uuid()->toString().'.'.$img->getClientOriginalExtension();
            tbl_collection_img_vehicles::create([
                    'collection_img_vehicles_url'=> $custom_name,
                    'vehicles_id' => $vehiculos->id,
            ]);
            $img->move(public_path().'/images_vehiculo',$custom_name);
            $cont++;
        }

        $licencia = $request->file('vehicles_licencia');
        $custom_name = 'licencia-'.Str::uuid()->toString().'.'.$licencia->getClientOriginalExtension();
        tbl_vehicles::where('vehicles_id', $vehiculos->id)->update([
            'vehicles_licencia'=> $custom_name,
        ]);
        $licencia->move(public_path().'/licencia_vehiculo',$custom_name);

        return ['status' => 'success', 'vehiculo' => $vehiculos ];
    }

}
