<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\tbl_category;
use App\Models\tbl_subcategory;

class CategoryController extends Controller
{
    public function GetCategory(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_category::all();
    }

    public function PostCategoria(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',            
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
            'category_name' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $category = tbl_category::create([
            'category_name' => $request->category_name
        ]);

        if ($category == true) {
            return ['status' => 200, 'message' => 'categoria guardada', 'category' => $category];
        }
        
    }

    public function GetSubCategoria(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_subcategory::with('category')->get();
    }

    public function PostSubCategoria(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $access_token = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($access_token->fails()) {
            return response()->json(['error' => $access_token->errors()], 401);
        }

        $validator = \Validator::make($request->all(),[
            'subcategory_name' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $subcategory = tbl_subcategory::create([
            'subcategory_name' => $request->subcategory_name,
            'category_id' => $request->category_id,
        ]);

        if ($subcategory == true) {
            return ['status' => 200, 'message' => 'Sub categoria guardada'];
        }
        
    }
}
