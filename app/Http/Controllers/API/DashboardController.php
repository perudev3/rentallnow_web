<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\tbl_renters;
use App\Models\tbl_hotel;
class DashboardController extends Controller
{

    public function index()
    {
        $title = [
            'primero' => 'Dashboard',
            'segundo' => 'Panel Principal',
        ];
        return view('home', compact('title'));
    }

    public function solicitudes_renters(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_renters::with('customers','user', 'category')->get();
    }

    public function detalle_renter(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        if ($request->category_id==1) {
            return tbl_hotel::with(['city','img_hotel'])->where('renters_id', $request->renters_id)->get();
        }
    }

    public function autorizacion_renters(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $renter = tbl_renters::where('renters_id', $request->renters_id)->update([
            'renters_status' => 1
        ]);

        if ($renter == true) {
            return ['status' => 'success', 'message' => 'Renter Autorizado'];
        }
    }


}
