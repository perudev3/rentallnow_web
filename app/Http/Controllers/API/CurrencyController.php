<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tbl_currency;

class CurrencyController extends Controller
{
    public function PostCurrency(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];
        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
            'currency_name' => 'required',
            'currency_abr' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $currency = tbl_currency::create([
            'currency_name' => $request->currency_name,
            'currency_abr' => $request->currency_abr,
        ]);

        if ($currency == true) {
            return [ 'status' => 200 , 'message' => 'moneda creada', 'currency' => $currency ];
        }
    }

}
