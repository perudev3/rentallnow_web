<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\tbl_category;
use App\Models\tbl_subcategory;
use App\Models\tbl_country;
use App\Models\tbl_city;
use App\Models\tbl_inmuebles;
use App\Models\tbl_collection_img_inmuebles;

class InmueblesController extends Controller
{
    public function categories(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_category::all();
    }

    public function subcategories(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_subcategory::where('category_id', $request->category_id)->get();
    }

    public function country(Request $request)
    {
        //return tbl_country::all();
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return Http::get('http://country.io/capital.json');
    }

    public function city(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_city::all();
    }

    public function UploatImageInmueble(Request $request)
    {
        $date = Carbon::now();
        $user = \Auth::user(); 

        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
            'inmuebles_date' => 'required',
            'subcategory_id' => 'required',
            'inmuebles_lat' => 'required',
            'inmuebles_lng' => 'required',
            'inmuebles_huespedes' => 'required',
            'inmuebles_cama' => 'required',
            'inmuebles_habitaciones' => 'required',
            'inmuebles_baños' => 'required',
            'inmuebles_modcons' => 'required',
            'user_id' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $inmuebles = tbl_inmuebles::create([
            'inmuebles_date' => Carbon::parse($date->format('d-m-Y')),
            'subcategory_id' => $request->subcategory_id,
            'inmuebles_lat' => $request->inmuebles_lat,
            'inmuebles_lng' => $request->inmuebles_lng,
            'inmuebles_huespedes' => $request->inmuebles_huespedes,
            'inmuebles_cama' => $request->inmuebles_cama,
            'inmuebles_habitaciones' => $request->inmuebles_habitaciones,
            'inmuebles_baños' => $request->inmuebles_baños,
            'inmuebles_modcons' => $request->inmuebles_modcons,
            'user_id' => $user->id,
        ]);

        $images = $request->file('collection_img_inmuebles_url');
        $cont = 0;
        foreach($images as $img){
            $custom_name = 'images-'.Str::uuid()->toString().'.'.$img->getClientOriginalExtension();
            $hotel_img = tbl_collection_img_inmuebles::create([
                    'collection_img_inmuebles_url'=> $custom_name,
                    'inmuebles_id' => $inmuebles->id,
            ]);
            $img->move(public_path().'/images_inmuebles',$custom_name);
            $cont++;
        }

        return ['status' => 'success', 'inmuebles_id' => $inmuebles->id ];

    }


    public function PostInmueble(Request $request)
    {
            $mensaje = [
                'access_token.required' => 'El código de autorización es obligatorio.',
            ];

            $validator = \Validator::make($request->all(),[
                'access_token' => 'required',
                'inmuebles_name' => 'required',
                'inmuebles_description' => 'required',
                'inmuebles_price' => 'required',
                'inmuebles_city' => 'required',
            ], $mensaje);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $inmuebles = tbl_inmuebles::where('inmuebles_id', $request->inmuebles_id)->update([
                'inmuebles_name' => $request->inmuebles_name,
                'inmuebles_description' => $request->inmuebles_description,
                'inmuebles_price' => $request->inmuebles_price,
                'inmuebles_city' => $request->inmuebles_city,
            ]);

            if ($inmuebles == true) {
                return ['status'=>'success' , 'mensaje' => 'Registro exitoso'];
            }             

    }
}
