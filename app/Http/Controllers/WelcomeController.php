<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\tbl_inmuebles;
use App\Models\tbl_vehicles;
use App\Models\tbl_reservas;
use App\Models\tbl_category;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function intro()
    {
        return view('renter');
    }

    public function property_group()
    {
        return view('renter');
    }

    public function categories()
    {
        return tbl_category::all();
    }

    public function GetRenters()
    {
        $inmuebles = tbl_inmuebles::with('img_inmuebles')->get();
        $vehiculos = tbl_vehicles::with('img_vehicles')->get();

        return [
            'inmuebles' => $inmuebles,
            'vehiculos' => $vehiculos,
        ];
    }

    public function CardSearch(Request $request)
    {
            if ($request->category_id == 1) {
                return tbl_inmuebles::with('img_inmuebles')->get();
            }
    }

    public function SearchInmueble(Request $request)
    {
        $inmueble = tbl_inmuebles::with('img_inmuebles')
                            ->where('inmuebles_lng', $request->hotel_lng)
                            ->whereNotExists(function($query) use($request){
                                $query->from('tbl_reservas')
                                      ->where('reservas_llegada','>=',  Carbon::parse($request->reserva_llegada))
                                      ->where('reservas_salida', '<=', Carbon::parse($request->reserva_salida))
                                      ->whereRaw('tbl_inmuebles.inmuebles_id = tbl_reservas.renters_id');
                            })->get(); 
        
        if ($inmueble == true) {
            return $inmueble;
        }else{
            return ['message' => 'No se encontro ningun inmueble para esas fechas'];
        }
    }

}