<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_renters;
use App\Models\tbl_category;
use App\Models\tbl_collection_img_article;

class tbl_article extends Model
{

    protected $fillable = [
        'article_date',
        'article_name',
        'article_price',
        'category_id',
        'renters_id'
    ];

    protected $primarykey = 'article_id';

    public function category()
    {
        return $this->hasMany(tbl_category::class,'category_id', 'category_id');
    }

    public function renters()
    {
        return $this->belongsTo(tbl_renters::class,'renters_id', 'renters_id');
    }

    public function img_article()
    {
        return $this->belongsTo(tbl_collection_img_article::class,'article_id', 'article_id');
    }

}
