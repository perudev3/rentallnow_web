<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_collection_img_inmuebles extends Model
{
    protected $fillable = [
        'collection_img_inmuebles_url',
        'inmuebles_id'
    ];

    protected $primarykey = 'collection_img_inmuebles_id';
}
