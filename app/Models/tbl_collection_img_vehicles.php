<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_collection_img_vehicles extends Model
{
    protected $fillable = [
        'collection_img_vehicles_url',
        'vehicles_id'
    ];

    protected $primarykey = 'collection_img_vehicles_id';
}
