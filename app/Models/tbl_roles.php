<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_roles extends Model
{
    protected $fillable = [
        'roles_name'
    ];

    protected $primarykey = 'roles_id';
}
