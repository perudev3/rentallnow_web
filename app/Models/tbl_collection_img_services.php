<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_collection_img_services extends Model
{
    protected $fillable = [
        'collection_img_services_url',
        'services_id'
    ];

    protected $primarykey = 'collection_img_services_id';
}
