<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class tbl_orders extends Model
{
    protected $fillable = [
        'orders_date',
        'orders_lat',
        'orders_lng',
        'user_id'
    ];

    protected $primarykey = 'orders_id';

    public function user()
    {
        return $this->hasMany(User::class,'id','user_id');
    }
}
