<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_subcategory extends Model
{
    protected $fillable = [
        'subcategory_name',
        'category_id'
    ];

    protected $primarykey = 'subcategory_id';


    public function category()
    {
        return $this->hasMany(tbl_category::class, 'category_id','category_id');
    }

    
}
