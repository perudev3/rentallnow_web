<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_customers;
use App\Models\User;
use App\Models\tbl_category;

class tbl_renters extends Model
{
    protected $fillable = [
        'renters_date',
        'customers_id',
        'user_id',
        'category_id',
        'subcategory_id',
        'renters_status'
    ];

    protected $primarykey = 'renters_id';

    public function customers()
    {
        return $this->hasMany(tbl_customers::class,'customers_id', 'customers_id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id','user_id');
    }

    public function category()
    {
        return $this->hasMany(tbl_category::class, 'category_id','category_id');
    }

    public function hotel()
    {
        return $this->belongsTo(tbl_hotel::class, 'renters_id','renters_id');
    }
}
