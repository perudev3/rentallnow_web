<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_collection_img_vehicles;

class tbl_vehicles extends Model
{
    protected $fillable = [
        'vehicles_lat',
        'vehicles_lng',
        'vehicles_year',
        'vehicles_make',
        'vehicles_model',
        'vehicles_odometers',
        'vehicles_transmission',
        'vehicles_type',
        'vehicles_maker_value',
        'vehicles_number',
        'vehicles_licencia',
        'vehicles_metas',
        'vehicles_disponibilidad',
        'vehicles_detalles',
        'subcategory_id',
        'user_id'
    ];

    protected $primarykey = 'vehicles_id';

    public function img_vehicles()
    {
        return $this->hasMany(tbl_collection_img_vehicles::class,'vehicles_id', 'vehicles_id');
    }
}
