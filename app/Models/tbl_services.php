<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_collection_img_services;

class tbl_services extends Model
{
    protected $fillable = [
        'services_city',
        'services_lat',
        'services_lng',
        'services_disponibilidad',
        'services_titulo',
        'services_detalles',
        'services_price',
        'subcategory_id',
        'user_id'
    ];

    protected $primarykey = 'services_id';

    public function img_services()
    {
        return $this->hasMany(tbl_collection_img_services::class,'services_id', 'services_id');
    }


}
