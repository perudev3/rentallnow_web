<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_country;
use App\Models\tbl_city;

class tbl_customers extends Model
{
    protected $fillable = [
        'customers_phone',
        'custumers_address',
        'country_id',
        'city_id'
    ];

    protected $primarykey = 'customers_id';

    public function country()
    {
        return $this->hasMany(tbl_country::class,'country_id', 'country_id');
    }

    public function city()
    {
        return $this->hasMany(tbl_city::class,'city_id', 'city_id');
    }
}
