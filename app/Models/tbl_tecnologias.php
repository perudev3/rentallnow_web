<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_collection_img_tecnologias;

class tbl_tecnologias extends Model
{
    protected $fillable = [
        'tecnologias_city',
        'tecnologias_lat',
        'tecnologias_lng',
        'tecnologias_disponibilidad',
        'tecnologias_titulo',
        'tecnologias_detalles',
        'tecnologias_price',
        'subcategory_id',
        'user_id'
    ];

    protected $primarykey = 'tecnologias_id';

    public function img_tecnologias()
    {
        return $this->hasMany(tbl_collection_img_tecnologias::class,'tecnologias_id', 'tecnologias_id');
    }

}
