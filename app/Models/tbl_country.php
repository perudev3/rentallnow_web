<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_currency;

class tbl_country extends Model
{

    protected $fillable = [
        'country_name',
        'currency_id'
    ];

    protected $primarykey = 'country_id';

    public function currency()
    {
        return $this->hasMany(tbl_currency::class,'currency_id', 'currency_id');
    }

}
