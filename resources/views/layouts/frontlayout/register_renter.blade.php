<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.min.css') }}" />
        <link  rel="stylesheet" type="text/css" href="{{ asset('css/glyphicon.css') }}"/>
        

        <!-- Styles -->
		<link rel="preconnect" href="https://fonts.gstatic.com/">
		<link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('css/vendors/bootstrap/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/vendors/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}"> 

        
        <style>
            ::-webkit-scrollbar {display: none;}
        </style>
    
</head>
<body>
    <div id="app">
        
            @yield('content')


    </div>

        <!-- Scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('js/zangdar.min.js') }}"></script>

        <link rel="stylesheet" type="text/css" href="{{ asset('js/intl-tel-input-13.0.0/build/css/intlTelInput.min.css') }}">

		<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/intl-tel-input-13.0.0/build/js/intlTelInput.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/isValidPhoneNumber.js') }}" type="text/javascript"></script>

    <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCv2upnuUOVx8ZuEIP1Dfd0IG1q6q4ZkdU&amp;language=en" async defer></script> -->
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/front.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/daterangecustom.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</body>
</html>
