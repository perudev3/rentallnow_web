<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.min.css') }}" />
        <link  rel="stylesheet" type="text/css" href="{{ asset('css/glyphicon.css') }}"/>
        

        <!-- Styles -->
		<link rel="preconnect" href="https://fonts.gstatic.com/">
		<link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('css/vendors/bootstrap/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/vendors/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}"> 

        

    
</head>
<body>
    <div id="app">
            <header class="header_area  animated fadeIn homenav">
                <div class="main_menu">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container-fluid container-fluid-90">
                            <!-- <a class="navbar-brand logo_h d-none d-sm-block" aria-label="logo" href="https://demowpthemes.com/buy2rental"><img src="public/front/images/logos/1628150190_logo.png" alt="logo" class="img-130x32"></a> 
                            <a class="navbar-brand logo_h d-block d-sm-none" aria-label="logo" href="https://demowpthemes.com/buy2rental"><img src="public/front/images/logos/1628150085_favicon.png" alt="logo" class="mob-logo"></a>  -->
                            <a href="#" onclick="goBack()" class="mob-back-btn d-block d-sm-none"><i class="fas fa-chevron-left"></i></a>
                            <!-- Trigger Button -->
                            <a href="#" aria-label="navbar" class="navbar-toggler" data-toggle="modal" data-target="#left_modal">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                          
                                
                            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                                <div class="nav navbar-nav menu_nav justify-content-end">
                                        @guest
                                        @else
                                            <div class="nav-item">
                                                    <a class="nav-link p-2 mt-3" href="{{ url('/intro') }}" aria-label="property-create">
                                                        <button class="btn button vbtn-default text-14 p-0 mt-2 pl-4 pr-4 br-50">
                                                            <p class="p-3 mb-0">  Hazte Renter</p>
                                                        </button>
                                                    </a>
                                            </div>
                                        @endif
                                        <div class="nav-item">
                                            <a class="nav-link globe" href="#" aria-label="modalLanguge" data-toggle="modal" data-target="#languageModalCenter"> 
                                                <i class="fa fa-globe text-18"></i> </a>
                                        </div>
                                    
                                        <div class="nav-item">
                                            <div class="dropdown sv_user_login">
                                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                        <img src="{{ asset('images/profile.jpg') }}" class="head_avatar" alt="">
                                                    </button>
                                                    
                                                        <ul class="dropdown-menu" >
                                                        @guest
                                                            <li><a href="{{ route('register') }}">Registrarse</a></li>
                                                            <li><a href="{{ route('login') }}">Login</a></li>  
                                                        @else
                                                            <li>
                                                                <a href="{{ url('/home') }}">Dashboard</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar Sesión</a>
                                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                                    @csrf
                                                                </form>
                                                            </li>
                                                        @endif
                                                        </ul>                                                    
                                            </div>

                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </nav>
                    
                    
                </div> 
            </header>

            <div class="modal right fade" id="left_modal" tabindex="-1" role="dialog" aria-labelledby="left_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header border-0 secondary-bg"> 
                            
                            <button type="button" class="close text-28" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="modal-body p-0">
                            <ul class="mobile-side">
                                @guest  
                                    <li><a href="{{ route('register') }}">Registrarse</a></li>
                                    <li><a href="{{ route('login') }}">Login</a></li>                                     
                                @else
                                    <li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
                                    <a class="mt-3" href="{{ url('/intro') }}">
                                        <button class="btn vbtn-outline-success text-14 font-weight-700 pl-5 pr-5 pt-3 pb-3">
                                                Hazte Renter
                                        </button>
                                    </a>
                                @endif
                                    
                                    
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        
            @yield('content')

            <footer class="main-panel card border footer-bg p-5" id="footer">
                <div class="container-fluid container-fluid-90 pb-5">
                    <div class="row">
                        <div class="col-6 col-sm-3 mt-4">
                            <h2 class="font-weight-600 text-uppercase text-13">Ciudades Populares</h2>
                            <div class="row">
                                <div class="col p-0">
                                    <ul class="mt-1">
                                        <li class="pt-3 text-14">
                                            <a href="search7777.html?location=New%20York&amp;checkin=05-09-2021&amp;checkout=05-09-2021&amp;guest=1">New York</a>
                                        </li>
                                                                    <li class="pt-3 text-14">
                                            <a href="search8c66.html?location=Sydney&amp;checkin=05-09-2021&amp;checkout=05-09-2021&amp;guest=1">Sydney</a>
                                        </li>
                                                                    <li class="pt-3 text-14">
                                            <a href="searcha95b.html?location=Paris&amp;checkin=05-09-2021&amp;checkout=05-09-2021&amp;guest=1">Paris</a>
                                        </li>
                                                                    <li class="pt-3 text-14">
                                            <a href="search60b5.html?location=Barcelona&amp;checkin=05-09-2021&amp;checkout=05-09-2021&amp;guest=1">Barcelona</a>
                                        </li>
                                                                    <li class="pt-3 text-14">
                                            <a href="search8d70.html?location=Berlin&amp;checkin=05-09-2021&amp;checkout=05-09-2021&amp;guest=1">Berlin</a>
                                        </li>     
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="col-6 col-sm-3 mt-4">
                            <h2 class="font-weight-600 text-uppercase text-13">Compañía</h2>
                            <div class="row">
                                <div class="col p-0">
                                    <ul class="mt-1">
                                        <li class="pt-3 text-14">
                                            <a href="policies.html">Politicas</a>
                                        </li>
                                        <li class="pt-3 text-14">
                                            <a href="privacy.html">Privacidad</a>
                                        </li>
                                                                                                    
                                    </ul>
                                </div>
                            </div>
                        </div>

                    
                        <div class="col-6 col-sm-3 mt-4">
                            <!-- <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="https://demowpthemes.com/buy2rental"><img src="public/front/images/logos/1628150190_logo.png" class="img-130x32" alt="logo"></a>
                                </div>
                            </div>-->
                        
                        </div>
                    </div>
                </div>

                <div class="border-top p-0 mt-4 foot-content">
                    <div class="container-fluid container-fluid-90 justify-content-between p-2 foot-padding">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 mb-0 mt-4 text-14">
                                © 2021. Todos los derechos reservados
                            </div>
                    
                    
                            <div class="col-lg-6 col-sm-6 mb-0 mt-4 text-14 text-right">
                                        <div class="text-underline mb-0">
                                            <a href="#" aria-label="modalLanguge" data-toggle="modal" data-target="#languageModalCenter"> <i class="fa fa-globe"></i> <u>English </u></a>
                                            <a href="#" aria-label="modalCurrency" data-toggle="modal" data-target="#currencyModalCenter"> <span class="ml-4">&#36; - <u>USD</u> </span></a>
                                            <ul class="list-inline text-center sv_social">
                                                                                                                                    <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="facebook"><i class="fab fa-facebook"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="google_plus"><i class="fab fa-google-plus"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="twitter"><i class="fab fa-twitter"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="linkedin"><i class="fab fa-linkedin"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="pinterest"><i class="fab fa-pinterest"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="youtube"><i class="fab fa-youtube"></i></a>
                                                    </li>
                                                                                            <li class="list-inline-item">
                                                    <a class="social-icon  text-color text-18" target="_blank" href="#" aria-label="instagram"><i class="fab fa-instagram"></i></a>
                                                    </li>
                                                                                            
                                            </ul>
                                        </div>
                                
                            
                            </div>
                        </div>
                    </div>
                    
                </div>
            </footer>

            <section class="footer-fixed-nav d-block d-sm-none"> 
                    <ul>
                        <li><a class="active-link" href="https://demowpthemes.com/buy2rental"><i class="fab fa-wpexplorer" aria-hidden="true"></i> <div class="icon-txt">Explore</div></a></li> 
                        <li><a class="" href="login.html" ><i class="far fa-heart" aria-hidden="true"></i> <div class="icon-txt">Saved</div></a></li>
                        <li><a class="" href="login.html" ><i class="far fa-paper-plane" aria-hidden="true"></i><div class="icon-txt">Trips</div> </a></li>
                        <li><a class="" href="login.html" ><i class="far fa-comment-alt" aria-hidden="true"></i><div class="icon-txt">Inbox</div> </a></li>
                        <li><a class="" href="login.html" ><i class="far fa-user" aria-hidden="true"></i><div class="icon-txt">Profile</div> </a></li>
                    </ul>
            </section> 

    </div>

        <!-- Scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('js/zangdar.min.js') }}"></script>

        <link rel="stylesheet" type="text/css" href="{{ asset('js/intl-tel-input-13.0.0/build/css/intlTelInput.min.css') }}">

		<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/intl-tel-input-13.0.0/build/js/intlTelInput.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/isValidPhoneNumber.js') }}" type="text/javascript"></script>

    <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCv2upnuUOVx8ZuEIP1Dfd0IG1q6q4ZkdU&amp;language=en" async defer></script> -->
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/front.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/daterangecustom.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</body>
</html>
