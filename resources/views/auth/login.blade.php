@extends('layouts.frontlayout.front')

@section('content')
    <div class="container-fluid user-login-section p-0">
        <div class="row">
            <div class="user-login" style="margin-top: 10px;">
                <div class="user-login-bg"></div>
                
                <div class="user-login-content">
                    <div class="user-login-form">
                            
                        <h2 class="text-center font-weight-600 welcome-txt">Bienvenido</h2>
                        <div class="d-flex justify-content-center login-list mt-5">
                            <div><a class="act-active">Login</a></div>
                        </div>
                        <div class="row mt-5 justify-content-center" >
                            <div class="col-lg-7 col-md-12">
                                <form class="mt-3" method="POST" action="{{ route('login') }}">  
                                    @csrf
                                    <div class="form-group col-sm-12 p-0 mt-3">
                                        <label class="font-weight-600">Email</label>
                                        <input type="email" class="form-control text-14 @error('email') is-invalid @enderror" placeholder = "Email" name="email" id="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                    
                                    <div class="form-group col-sm-12 p-0 mt-3">
                                        <label class="font-weight-600">Password</label>
                                        <input type="password" class="form-control text-14 @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="current-password" placeholder = "Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-3" >
                                        <div class="d-flex justify-content-between">
                                            <div class="m-3 text-14">
                                                @if (Route::has('password.request'))
                                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                                       <h5> {{ __('¿Olvidáste tu contraseña?') }} </h5>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-5" >
                                        <button type="submit" class="btn google_btn pt-3 pb-3 text-16 w-100 mt-3">
                                            Ingresar
                                        </button>
                                    </div>
                                </form>
                                <div class="or-sec mb-5 mt-5">
                                    <span class="or-section">ó</span>
                                </div>
                                <a href="{{ url('/auth/google') }}">
                                    <button class="btn google_btn pt-3 pb-3 text-16 w-100 mt-3">
                                    <span><i class="fab fa-google-plus-g  mr-2 text-16"></i>  Ingresa con Google</span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
