@extends('layouts.frontlayout.front')

@section('content')
    <div class="container-fluid user-login-section p-0">
        <div class="row">
            <div class="user-login" style="margin-top:10px;">
                <div class="user-login-bg"></div>
                
                <div class="user-login-content">
                    <div class="user-login-form">
                        <h2 class="text-center font-weight-600 welcome-txt">Regístrate es gratis</h2>
                        <div class="d-flex justify-content-center login-list mt-5">
                            <div><a class="act-active">Registrarse</a></div>
                        </div>
                        <div class="row mt-5 justify-content-center" >
                            <div class="col-lg-7 col-md-12">
                                <form class='signup-form login-form'  method="POST" action="{{ route('register') }}">    
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-sm-12 p-0 mt-3">
                                            <label class="font-weight-600">Nombre y Apellidos</label>
                                            <input type="text" class="form-control text-14 p-2 @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12 p-0 mt-3">
                                            <label class="font-weight-600">Teléfono</label>
                                            <input type="text" class="form-control text-14 p-2 @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-3">
                                        <label class="font-weight-600">Email</label>
                                        <input type="text" class="form-control text-14 p-2 @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-3">
                                        <label class="font-weight-600">Password</label>
                                        <input type="password" class="form-control text-14 p-2 @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="new-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-3">
                                        <label class="font-weight-600">Confirmar Contraseña</label>
                                        <input type="password" class='form-control text-14 p-2' id="password-confirm" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    <div class="form-group col-sm-12 p-0 mt-5" >
                                        <button type='submit' id="btn" class="btn pb-3 pt-3 text-15 rounded-3 vbtn-success w-100 ml-0 mr-0 mb-3"> <i class="spinner fa fa-spinner fa-spin d-none" ></i>
                                            <span id="btn_next-text">Registrarse</span>
                                        </button>
                                    </div>
                                    <a href="{{ route('login') }}" class="underline text-sm text-gray-600 hover:text-gray-900">
                                        ¿Ya tienes cuenta?, Login
                                    </a>
                                </form>
                                <div class="or-sec mb-5 mt-2">
                                    <span class="or-section">ó</span>
                                </div>
                                <a href="{{ url('/auth/google') }}">
                                    <button class="btn google_btn pt-3 pb-3 text-16 w-100 mt-3">
                                        <span><i class="fab fa-google-plus-g  mr-2 text-16"></i>  Ingrese con Google</span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
